"use strict";
const yargs = require("yargs");
const fs = require("fs-extra");
import walk from "walk";
import colors from "colors";
const path = require("path");
const yaml = require("js-yaml");
const jsome = require("jsome");
const glob = require("glob");
var rsync = require("rsync");
import { logger } from "mimer-logger";

const program_name = "htsit-order";
const program_dir = __dirname;
const path_config = `${program_dir}/config.yaml`;
const tmp_file = `${program_dir}/tmp.json`;

let arg = yargs
  .describe("v", "Give a verbose print")
  .alias("v", "verbose")
  .default("v", false)
  .describe("d", "Give a debug print")
  .alias("d", "debug")
  .default("d", false)
  .describe("t", "Give a trace print")
  .alias("t", "trace")
  .default("t", false)
  .help("h")
  .alias("h", "help")
  .boolean(["d", "v", "t", "h", "y", "s", "T", "c", "n", "a"])
  .epilog("Copyright 2019 by Magnus Kronnäs").argv;

// Log

let log_level = 5;

if (arg.verbose) {
  log_level = 5;
}

if (arg.debug) {
  log_level = 6;
}

if (arg.trace) {
  log_level = 7;
}

const log = new logger(log_level);

log.debug(arg);

let question = arg._;

// Select mode of program
let mode = "search";

log.debug(question);

// Aiding function

function orderToText(data) {
  /*

  // Tags as icons
  let icon = "";
  if (data.type) {
    switch (data.type) {
      case "student":
        icon += "🦉";
        break;
    }
  }

  if (data.status) {
    switch (data.status) {
      case "quiet":
        icon += "😴";
        break;
    }
  }

  // Degress

  let degrees = "";
  if (data.course) {
    for (const item of data.course) {
      degrees += colors.bold(item.name) + ": " + colors.red(item.degree) + "\n";
    }
  }
*/
  // Make result text

  let result = "";
  result += "        " + colors.green.underline(data.name) + "\n";
  result += colors.italic(data.status) + "\n";
  result += colors.bold(data.supplier) + "\n";
  result += "test" + "\n\n";
  result += "📅 " + colors.blue(data.budget) + "\n";

  return result;
}

function showAlternatives(orderList) {
  // Show list for selection
  log.debug(orderList[0]);

  let text = "";
  for (let i = 0; i < orderList.length; i++) {
    text += colors.bold(i) + " : " + colors.green(orderList[i].name) + "\n";
  }
  return text;
}

function showOrder(orderPath) {
  log.debug(orderPath);
  return fs
    .readFile(orderPath, { encoding: "utf-8" })
    .then(yaml.safeLoad)
    .then(data => {
      return orderToText(data);
    });
}

// Variabels for stats

let count = 0;
let cache = { member: [] };
let member = [];

// Main function

function main(configPath) {
  log.debug(configPath);
  return fs
    .readFile(configPath, { encoding: "utf-8" })
    .then(yaml.safeLoad)
    .then(config => {
      const data_dir = config.data;
      log.debug(data_dir);

      switch (mode) {
        case "selection":
          log.debug("selection");
          fs.readFile(tmp_file, { encoding: "utf-8" })
            .then(JSON.parse)
            .then(selection => {
              if (question < selection.length) {
                showOrder(selection[question].path).then(log.log);
              } else {
                log.error("No item exsist for index.");
              }
            });
          break;
        case "search":
          const walker = walk.walk(data_dir);
          const order_list = [];

          walker.on("file", function(root, fileStats, next) {
            log.trace(fileStats.name);
            fs.readFile(path.join(root, fileStats.name), { encoding: "utf-8" })
              .then(yaml.safeLoad)
              .then(data => {
                count++;
                order_list.push({
                  path: path.join(root, fileStats.name),
                  name: data.name
                });
                next();
              });
          });

          walker.on("errors", function(root, nodeStatsArray, next) {
            log.error("File can not be found");
            next();
          });

          walker.on("end", function() {
            log.debug("All done");
            log.log(showAlternatives(order_list));
            // Save for later use
            fs.writeFile(tmp_file, JSON.stringify({ data: order_list }));
            log.log("Number of orders: " + count);
          });
          break;
      }
    });
}

main(path_config);
